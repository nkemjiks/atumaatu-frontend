import history from './history';

export const nav = (location) => {
  history.push(location);
};

