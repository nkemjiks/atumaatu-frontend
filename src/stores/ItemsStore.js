import { observable, action, decorate } from "mobx";
import { createContext } from "react";
import { makeAuthenticatedApiCall } from "../helpers/api";

class ItemsStore {
  state = {
    items: [],
    token: '',
    errors: {},
    kind: 'loading'
  };

  getItems = () => {
    makeAuthenticatedApiCall(`/v1/items`, {}, 'get', this.state.token)
      .then(res => {
        if (res && res.errorMessage) {
          this.updateErrorsState(res);
        } else {
          this.updateState(res);
        }
      });
  };

  addItem = (detail) => {
    this.updateKindState('loading');
    makeAuthenticatedApiCall(`/v1/items`, { item: { name: detail.name} }, 'post', this.state.token)
      .then(res => {
        if (res && res.errorMessage) {
          if (res.errorMessage.name) {
            this.updateErrorsState({ errorMessage: 'Name has already been taken'})
          } else {
            this.updateErrorsState(res);
          }
          this.updateKindState('ready');
        } else {
          this.updateState(res);
        }
      });
  }

  editItem = (id, detail) => {
    this.updateKindState('loading');
    makeAuthenticatedApiCall(`/v1/items/${id}`, { item: { name: detail.name} }, 'put', this.state.token)
      .then(res => {
        if (res && res.errorMessage) {
          this.updateErrorsState(res);
        } else {
          this.updateState(res);
        }
      });
  }

  removeItem = (id) => {
    this.updateKindState('loading');
    makeAuthenticatedApiCall(`/v1/items/${id}`, {}, 'delete', this.state.token)
      .then(res => {
        if (res && res.errorMessage) {
          this.updateErrorsState(res);
        } else {
          this.updateState(res);
        }
      });
  }

  updateErrorsState = (res) => {
    this.updateErrorState(res);
    this.updateKindState('ready');
  }

  updateState = (res) => {
    this.updateItemsState(res.data.payload.items);
    this.updateKindState('ready');
  }

  updateKindState(kind) {
    this.state = {
      ...this.state,
      kind: kind
    }
  }

  updateTokenState(token) {
    this.state = {
      ...this.state,
      token: token
    }
  }

  updateItemsState(items) {
    this.state = {
      ...this.state,
      items: items
    }
  }

  updateErrorState(errors) {
    this.state = {
      ...this.state,
      errors: errors
    }
  }
}

decorate(ItemsStore, {
  state: observable,
  getItems: action,
  addItem: action,
  removeItem: action,
  editItem: action,
  updateTokenState: action,
  updateKindState: action,
  updateErrorState: action,
  updateItemsState: action,
  updateState: action
})

export const ItemsStoreContext = createContext(new ItemsStore());