import { observable, action, decorate } from "mobx";
import { createContext } from "react";
import { makeAuthenticatedApiCall } from "../helpers/api";

class BudgetItemsStore {
  state = {
    budget: {},
    items: [],
    token: '',
    errors: {},
    kind: 'loading'
  };

  getUserBudgetItems = (budget_id) => {
    makeAuthenticatedApiCall(`/v1/budget_memberships/${budget_id}`, {}, 'get', this.state.token)
      .then(res => {
        if (res && res.errorMessage) {
          this.updateErrorState(res);
        } else {
          this.updateState(res);
        }
      });
  };

  addItems = (detail) => {
    this.updateKindState('loading');
    makeAuthenticatedApiCall(`/v1/budget_memberships`, detail, 'post', this.state.token)
      .then(res => {
        if (res && res.errorMessage) {
          this.updateErrorState(res);
        } else {
          this.updateState(res);
        }
      });
  }

  editItem = (budget_id, detail) => {
    makeAuthenticatedApiCall(`/v1/budget_memberships/${budget_id}`, detail, 'put', this.state.token)
      .then(res => {
        if (res && res.errorMessage) {
          this.updateErrorState(res);
        } else {
          this.updateState(res);
        }
      });
  }

  removeItem = (budget_id, detail) => {
    makeAuthenticatedApiCall(`/v1/budget_memberships/${budget_id}`, detail, 'delete', this.state.token)
      .then(res => {
        if (res && res.errorMessage) {
          this.updateErrorState(res);
        } else {
          this.updateState(res);
        }
      });
  }

  updateState = (res) => {
    this.updateBudgetItemsState(res.data.payload.items);
    this.updateBudgetInfoState(res.data.payload.budget);
    this.updateKindState('ready')
  }

  updateKindState(kind) {
    this.state = {
      ...this.state,
      kind: kind
    }
  }

  updateTokenState(token) {
    this.state = {
      ...this.state,
      token: token
    }
  }

  updateBudgetItemsState(items) {
    this.state = {
      ...this.state,
      items: items
    }
  }

  updateBudgetInfoState(budget) {
    this.state = {
      ...this.state,
      budget: budget
    }
  }

  updateTotalCostState(totalCost) {
    this.state = {
      ...this.state,
      totalCost: totalCost
    }
  }

  updateErrorState(errors) {
    this.state = {
      ...this.state,
      errors: errors
    }
  }
}

decorate(BudgetItemsStore, {
  state: observable,
  getUserBudgetItems: action,
  addItems: action,
  removeItem: action,
  editItem: action,
  updateTokenState: action,
  updateKindState: action,
  updateErrorState: action,
  updateBudgetItemsState: action,
  updateTotalCostState: action,
  updateBudgetInfoState: action,
  updateState: action
})

export const BudgetItemsStoreContext = createContext(new BudgetItemsStore());