import { observable, action, decorate } from "mobx";
import { createContext } from "react";
import { makeAuthenticatedApiCall } from "../helpers/api";
import { nav } from "../nav";

class BudgetStore {
  state = {
    budgets: [],
    token: '',
    errors: {},
    kind: 'loading'
  };

  getUserBudgets = () => {
    makeAuthenticatedApiCall('/v1/budgets', {}, 'get', this.state.token)
      .then(res => {
        if (res && res.data.payload) {
          this.updateState(res);
        } else {
          this.updateErrorState(res);
          this.updateKindState('ready');
        }
      });
  };

  addBudget = (detail) => {
    this.updateKindState('loading')
    makeAuthenticatedApiCall('/v1/budgets', detail, 'post', this.state.token)
      .then(res => {
        if (res && res.data) {
          nav(`/budget/${res.data.id}`);
        } else {
          this.updateErrorState(res);
          this.updateKindState('ready');
        }
      });
  }

  copyBudget = (id) => {
    this.updateKindState('loading')
    makeAuthenticatedApiCall('/v1/budgets/copy', { existing_budget_id: id }, 'put', this.state.token)
      .then(res => {
        if (res && res.data) {
          this.updateState(res);
        } else {
          this.updateErrorState(res);
          this.updateKindState('ready');
        }
      });
  }

  editBudget = (id, detail) => {
    this.updateKindState('loading')
    makeAuthenticatedApiCall(`/v1/budgets/${id}`, detail, 'put', this.state.token)
    .then(res => {
        if (res && res.data) {
          this.updateState(res);
        } else {
          this.updateErrorState(res);
          this.updateKindState('ready');
        }
      });
  }

  deleteBudget = (id) => {
    this.updateKindState('loading')
    makeAuthenticatedApiCall(`/v1/budgets/${id}`, {}, 'delete', this.state.token)
      .then(res => {
        if (res && res.data) {
          this.updateState(res);
        } else {
          this.updateErrorState(res);
          this.updateKindState('ready');
        }
      });
  }

  updateState = (res) => {
    this.updateBudgetState(res.data.payload.budgets);
    this.updateKindState('ready')
  }

  updateKindState(kind) {
    this.state = {
      ...this.state,
      kind: kind
    }
  }

  updateTokenState(token) {
    this.state = {
      ...this.state,
      token: token
    }
  }

  updateBudgetState(budgets) {
    this.state = {
      ...this.state,
      budgets: budgets
    }
  }

  updateErrorState(errors) {
    this.state = {
      ...this.state,
      errors: errors
    }
  }
}

decorate(BudgetStore, {
  state: observable,
  getUserBudgets: action,
  editBudget: action,
  addBudget: action,
  deleteBudget: action,
  updateTokenState: action,
  updateKindState: action,
  updateErrorState: action,
  updateBudgetState: action,
  updateState: action
})

export const BudgetStoreContext = createContext(new BudgetStore());