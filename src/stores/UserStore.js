import { observable, action, decorate, computed } from "mobx";
import { createContext } from "react";
import { makeApiCall, makeAuthenticatedApiCall } from "../helpers/api";
import { nav } from "../nav";

export class UserStore {
  state = {
    user:  {
      firstName: '',
      id: undefined,
      lastName: '',
      email: '',
      currency: 'NGN',
      token: ''
    },
    errors: {},
    kind: 'loading'
  };

  loginUser = (details) => {
    makeApiCall('/v1/sessions', details, 'post')
      .then(res => {
        if (res && res.errorMessage) {
          this.updateErrorState(res);
        } else {
          this.updateState(res);
        }
      });
  };

  signUpUser = (details) => {
    makeApiCall('/v1/users', {user: details}, 'post')
      .then(res => {
        if (res && res.errorMessage) {
          this.updateErrorState(res);
        } else {
          this.updateState(res);
        }
      });
  };

  updateCurrency = (detail) => {
    this.updateKindState('loading');
    makeAuthenticatedApiCall(`/v1/sessions/${this.state.user.id}`,  { user: detail }, 'put', this.state.user.token)
      .then(res => {
        if (res && res.errorMessage) {
          this.updateErrorsState(res);
        } else {
          this.updateState(res, '/profile');
        }
      });
  }

  storeUserInLocalStorage(user) {
    if (user) {
      localStorage.setItem('user', JSON.stringify(user));
    }
  }

  logOutUser() {
    localStorage.removeItem('user');
    this.state = {
      ...this.state,
      user:  {
        firstName: '',
        id: undefined,
        lastName: '',
        email: '',
        currency: 'NGN',
        token: ''
      }
    }
    nav('/');
  }

  getUserFromLocalStorage(url = '/dashboard') {
    const user = localStorage.getItem('user');
    if (user) {
      this.updateKindState('ready');
      this.updateUserState(JSON.parse(user));
      nav(url);
    }
    this.updateKindState('login');
  }

  updateState(res, url = '/dashboard') {
    this.updateUserState(res.data.payload);
    this.storeUserInLocalStorage(res.data.payload);
    this.updateKindState('ready');
    nav(url);
  }

  updateKindState(kind) {
    this.state = {
      ...this.state,
      kind: kind
    }
  }

  updateUserState(user) {
    this.state = {
      ...this.state,
      user: user
    }
  }

  updateErrorState(errors) {
    this.state = {
      ...this.state,
      errors: errors
    }
  }

  get isLoggedIn() {
    if(this.state.kind === "ready") {
      return true;
    }
    return false;
  }
}

decorate(UserStore, {
  state: observable,
  loginUser: action,
  signUpUser: action,
  updateState: action,
  storeUserInLocalStorage: action,
  getUserFromLocalStorage: action,
  updateUserState: action,
  updateKindState: action,
  updateErrorState: action,
  logOutUser: action,
  isLoggedIn: computed
})

export const UserStoreContext = createContext(new UserStore());