import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import InternalServer from './components/ErrorPages/InternalServer';
import Login from './components/Login';
import Signup from './components/Signup';
import history from './history';
import AppRouter from './AppRouter';

const MainAppRouter = () => {
  return (
    <Router history={history}>
      <Switch>
        <Route exact path="/" component={Login}/>
        <Route exact path="/signup" component={Signup}/>
        <Route path="/500" component={InternalServer} />
        <Route path="*" component={AppRouter} />
      </Switch>
    </Router>
  );
}

export default MainAppRouter;