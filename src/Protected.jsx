import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export const UserProtected = ({ component: Component, path }) => {
  const user = JSON.parse(localStorage.getItem('user'));

  return (
    <Route
      path={path}
      render={({ location }) => (
        user && user.token
        ? <Component />
        : <Redirect to={{ pathname: '/', state: { from: location } }} />
      )}
    />
  );
};