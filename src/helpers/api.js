import axios from 'axios';
import { nav } from "../nav";

export async function makeApiCall(path, data, method) {
  const api = process.env.REACT_APP_API_ENDPOINT;

  try {
    const responseData = await axios({
      method: method,
      url: `${api}${path}`,
      data: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      }
    })
    return responseData;
  } catch (error) {
    if((error.response === undefined) || (error.response.status === 500)) {
      return nav('/500');
    } else {
      return { errorMessage: error.response.data.error };
    }
  }
}

export async function makeAuthenticatedApiCall(path, data, method, token) {
  const api = process.env.REACT_APP_API_ENDPOINT;
  try {
    const responseData = await axios({
      method: method,
      url: `${api}${path}`,
      data: data && JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${token}`
      }
    })
    return responseData;
  } catch (error) {
    if ((error.response === undefined) || (error.response.status === 500)) {
      return nav('/500');
    } else {
      return { errorMessage: error.response.data.errorMessage };
    }
  }
}
