/* eslint-disable */
// https://github.com/styled-components/styled-components/blob/master/packages/styled-components/docs/tips-and-tricks.md
import { css } from 'styled-components';

const sizes = {
  mobile: 425,
  tablet: 768,
  laptopSmall: 1024,
  laptop: 1280,
  desktop: 1440,
  extraLarge: 1840
};

const mediaQueries = Object.keys(sizes).reduce(
  (accumulator, label) => {
    // @ts-ignore
    const emSize = sizes[label] / 16;

    accumulator[label] = (...args) => css`
      @media (min-width: ${emSize}em) {
        // @ts-ignore
        ${css(...args)};
      }
    `;
    return accumulator;
  },
  {}
);

export default mediaQueries;