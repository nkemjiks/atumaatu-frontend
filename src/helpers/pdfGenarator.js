import { cost } from "./calc";

export const docDefinition = (title, budgetCost, currency, items) => {
  return {
    content: [
      {text: `${title}`, style: 'header'},
      {text: `Total Cost: ${cost(currency, budgetCost)}`, style: 'subheader'},
      {
        style: 'tableExample',
        table: {
          widths: ['*', 'auto'],
          heights: 30,
          body: items,
        }
      }
    ],
    styles: {
      header: {
        fontSize: 20,
        bold: true,
        marginBottom: 4,
        color: '#0074c1'
      },
      subheader: {
          fontSize: 12,
          marginBottom: 10,
          bold: true,
      }
    }
  }
};
