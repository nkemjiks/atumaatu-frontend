export const cost = (currency, x) => {
  return `${currency} ${x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
};