import React from "react";

const Difference = ({ item, costDifference }) => {
  const diff = item.budgetedCost - item.actualCost;

  return (
    (diff >= 0) ? (
    <span className="text-green-500">{costDifference}</span>
    ) : (<span className="text-red-500">{costDifference}</span>)
  );
};

export const DifferenceExpectedBalance = ({ budget, costDifference }) => {
  const diff = budget.income - budget.budgetedCost;

  return (
    (diff >= 0) ? (
    <span className="text-green-500">{costDifference}</span>
    ) : (<span className="text-red-500">{costDifference}</span>)
  );
};

export const DifferenceActualBalance = ({ budget, costDifference }) => {
  const diff = budget.income - budget.actualCost;

  return (
    (diff >= 0) ? (
    <span className="text-green-500">{costDifference}</span>
    ) : (<span className="text-red-500">{costDifference}</span>)
  );
};

export default Difference;
