import React from "react";
import styled from "styled-components";
import Modal from "react-responsive-modal";

const Container = styled.div`
  width: auto;

  h2 {
    margin-bottom: 20px;
    font-size: 20px;
    text-align: center;
    margin-top: 15px;
    font-weight: 700;
  }
`;
const AppModal = ({ title, children, open, onCloseModal }) => {
  return (
    <Modal
      open={open}
      onClose={onCloseModal}
      center
      closeOnOverlayClick={false}
      closeOnEsc={false}
    >
      <Container>
        <h2>{title}</h2>
        <div>{children}</div>
      </Container>
    </Modal>
  );
};

export default AppModal;
