import React, { useEffect, useContext, useState } from 'react';
import { observer } from 'mobx-react';
import {withRouter} from 'react-router';
import * as yup from "yup";
import { FaEdit } from "react-icons/fa";
import { UserStoreContext } from "../../stores/UserStore";
import Loading from '../shared/LoadingOverlay';
import EditProfile from './EditProfile';

const validationSchema = yup.object({
  currency: yup.string().required("Currency is required")
});

const Profile = observer(({ match }) => {
  const userStore = useContext(UserStoreContext);
  const [open, setOpen] = useState(false);

  const { kind, errors, user } = userStore.state;

  useEffect(() => {
    userStore.getUserFromLocalStorage(match.url);
    document.title = 'Profile - Atumaatu'
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const onOpenModal = () => {
    setOpen(true);
  };

  const onCloseModal = () => {
    setOpen(false);
  };

  const handleSubmit = (detail) => {
    console.log(detail);
    userStore.updateCurrency(detail);
    onCloseModal()
  }

  return kind === "loading" ?
    <Loading /> :
    (
      <div className="bg-gray-100 min-h-screen px-12 py-2">
        <EditProfile
          title={'Change Currency'}
          open={open}
          onCloseModal={onCloseModal}
          initialValues={{ currency: user.currency }}
          validationSchema={validationSchema}
          handleSubmit={handleSubmit}
        />
        {errors && errors.errorMessage && (
          <p className=" text-center mb-2 text-red-500 text-xs italic">
            {errors.errorMessage}
          </p>
        )}
        <div className="mb-4 mt-4">
          <h4 className="font-bold text-xl mb-2">Name</h4>
          <p>{user.firstName} {user.lastName}</p>
        </div>
        <div className="mb-4">
          <h4 className="font-bold text-xl mb-2">Email</h4>
          <p>{user.email}</p>
        </div>
        <div className="mb-4">
        <h4 className="font-bold text-xl mb-2">Currency</h4>
        <div className="flex">
          <p className="mr-2">{user.currency}</p>
          <FaEdit className="text-blue-700 mt-1" onClick={onOpenModal}/>
        </div>
        </div>
      </div>
    )
});

export default withRouter(Profile);