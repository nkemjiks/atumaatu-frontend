import React from "react";
import AppModal from "../../shared/Modal";
import EditProfileForm from "./EditProfileForm";

const EditItem = ({ title, open, onCloseModal, handleSubmit, initialValues, validationSchema, errors }) => {
  return (
    <AppModal
      title={title}
      open={open}
      onCloseModal={onCloseModal}
    >
      <EditProfileForm
        initialValues={initialValues}
        validationSchema={validationSchema}
        handleSubmit={handleSubmit}
        errors={errors}
      />
    </AppModal>
  );
};

export default EditItem;
