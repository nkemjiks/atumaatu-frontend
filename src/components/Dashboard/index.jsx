import React, { useContext, useEffect, useState } from "react";
import { observer } from "mobx-react";
import { FaEdit, FaTrash, FaCopy } from "react-icons/fa";
import * as yup from "yup";
import { UserStoreContext } from "../../stores/UserStore";
import { BudgetStoreContext } from "../../stores/BudgetStore";
import AppModal from "../shared/Modal";
import BudgetForm from "./BudgetForm";
import Loading from "../shared/LoadingOverlay";
import { cost } from "../../helpers/calc";
import Difference, { DifferenceExpectedBalance, DifferenceActualBalance } from "../shared/Difference";

const initialValues = {
  name: "",
  income: undefined
};

const validationSchema = yup.object({
  name: yup
    .string()
    .max(256, "Name must be 256 characters or less")
    .required("Name is required"),
  income: yup
    .number()
    .min(1)
    .required("Income is required")
});

const Dashboard = observer(() => {
  const userStore = useContext(UserStoreContext);
  const budgetStore = useContext(BudgetStoreContext);
  const { errors, kind, budgets } = budgetStore.state;
  const [open, setOpen] = useState(false);
  const [budgetKind, setBudgetKind] = useState("create");
  const [currentEditBudget, setCurrentEditBudget] = useState({});

  useEffect(() => {
    userStore.getUserFromLocalStorage();
    document.title = "Dashboard - Atumaatu";
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    budgetStore.updateTokenState(userStore.state.user.token);
    budgetStore.getUserBudgets();

    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userStore.state.user.token]);

  const getDate = (budgetDate) => {
    const date = new Date(budgetDate);
    const year = date.getFullYear();
    const month = date.getMonth();
    const day = date.getDate();
    const months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    return `${day} ${months[month]}, ${year}`;
  };

  const onOpenModal = (kind) => {
    setBudgetKind(kind);
    setOpen(true);
  };

  const onCloseModal = () => {
    setOpen(false);
    initialValues.name = "";
  };

  const handleSubmit = (event) => {
    if (budgetKind === "create") {
      budgetStore.addBudget({ name: event.name, budgeted_cost: 0, income: event.income });
    } else if (budgetKind === "edit") {
      budgetStore.editBudget(currentEditBudget.id, {
        name: event.name,
        budgeted_cost: currentEditBudget.budgetedCost,
        income: event.income
      });
    }
    onCloseModal();
  };

  const handleDelete = (id) => {
    budgetStore.deleteBudget(id);
  };

  const handleEdit = (budget) => {
    initialValues.name = budget.name;
    initialValues.income = budget.income;
    setCurrentEditBudget({ id: budget.id, budgetedCost: budget.budgetedCost, income: budget.income });
    onOpenModal("edit");
  };

  const handleCopy = (id) => {
    budgetStore.copyBudget(id);
  };

  return kind === "loading" ? (
    <Loading />
  ) : (
    <div className="bg-gray-100 min-h-screen">
      <AppModal
        title={"Create a new budget"}
        open={open}
        onCloseModal={onCloseModal}
      >
        <BudgetForm
          initialValues={initialValues}
          validationSchema={validationSchema}
          handleSubmit={handleSubmit}
          errors={errors}
          onCloseModal={onCloseModal}
          buttonActionName={
            budgetKind === "create" ? "Create Budget" : "Update Budget"
          }
        />
      </AppModal>
      <div className="flex justify-end py-8 px-12">
        <button
          onClick={() => onOpenModal("create")}
          className="bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow"
        >
          + Create new Budget
        </button>
      </div>

      <div className="px-12">
        <h1 className="font-bold text-4xl mb-2">Budgets</h1>
        {errors && errors.errorMessage && (
          <p className=" text-center mb-2 text-red-500 text-xs italic">
            {errors.errorMessage}
          </p>
        )}
        {budgets.length <= 0 ? (
          <p className="italic">You don't have a budget yet. Create a budget</p>
        ) : (
          <div className="flex justify-between flex-wrap py-8 ">
            {budgets.map((budget, i) => {
              return (
                <div
                  key={i}
                  className="max-w-sm w-1/2 sm:w-full md:w-1/2 lg:1/3 xl:w-1/5 rounded overflow-hidden shadow-lg m-4"
                >
                  <div className="px-10 py-10 bg-gray-300">
                    <h1 className="font-bold text-3xl text-center mb-2">
                      {cost(userStore.state.user.currency, budget.budgetedCost)}
                    </h1>
                  </div>
                  <div className="px-6 py-4">
                    <a
                      className="font-bold text-xl mb-2 hover:text-blue-700"
                      href={`/budget/${budget.id}`}
                    >
                      {budget.name}
                    </a>
                    <p className="text-gray-700 text-base mb-1">
                      {budget.itemCount} Items
                    </p>
                    <p className="text-gray-700 text-base mb-1">
                      <strong>Income: </strong>
                      <span>
                        {cost(userStore.state.user.currency, budget.income)}
                      </span>
                    </p>
                    <p className="text-gray-700 text-base mb-1">
                      Actual Cost:{" "}
                      <span>
                        {cost(userStore.state.user.currency, budget.actualCost)}
                      </span>
                    </p>
                    <p className="text-gray-700 text-base mb-1">
                      Difference:{" "}
                      <Difference
                        item={budget}
                        costDifference={cost(
                          userStore.state.user.currency,
                          budget.budgetedCost - budget.actualCost
                        )}
                      />
                    </p>
                    <p className="text-gray-700 text-base mb-1">
                      Expected Balance:{" "}
                      <DifferenceExpectedBalance
                        budget={budget}
                        costDifference={cost(
                          userStore.state.user.currency,
                          budget.income - budget.budgetedCost
                        )}
                      />
                    </p>
                    <p className="text-gray-700 text-base mb-1">
                      Actual Balance:{" "}
                      <DifferenceActualBalance
                        budget={budget}
                        costDifference={cost(
                          userStore.state.user.currency,
                          budget.income - budget.actualCost
                        )}
                      />
                    </p>
                  </div>
                  <div className="px-6 py-4 flex justify-between">
                    <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2">
                      {getDate(budget.createdAt)}
                    </span>
                    <div>
                      <span
                        onClick={() => handleEdit(budget)}
                        className="cursor-pointer inline-block rounded-full px-1 py-2 text-sm font-semibold text-right text-blue-700 mr-2"
                      >
                        <FaEdit />
                      </span>
                      <span
                        onClick={() => handleCopy(budget.id)}
                        className="cursor-pointer inline-block rounded-full px-1 py-2 text-sm font-semibold text-right text-blue-700 mr-2"
                      >
                        <FaCopy />
                      </span>
                      <span
                        onClick={() => handleDelete(budget.id)}
                        className="cursor-pointer inline-block rounded-full px-1 py-2 text-sm font-semibold text-right text-red-700 mr-2"
                      >
                        <FaTrash />
                      </span>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        )}
      </div>
    </div>
  );
});

export default Dashboard;
