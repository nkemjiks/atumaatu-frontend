import React from "react";
import { Form, Formik, Field, ErrorMessage } from "formik";

const AddBudgetForm = ({
  initialValues,
  validationSchema,
  handleSubmit,
  errors,
  onCloseModal,
  buttonActionName,
}) => {
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={handleSubmit}
    >
      <div className="w-full mx-auto self-center">
        <Form className="bg-white px-8 pt-6 pb-8 mb-4">
          {errors && errors.errorMessage && (
            <p className=" text-center mb-2 text-red-500 text-xs italic">
              {errors.errorMessage}
            </p>
          )}
          <div className="mb-4">
            <label className="block text-gray-700 text-sm mb-2" htmlFor="email">
              Budget Name
            </label>
            <Field
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
              id="name"
              placeholder="Budget name"
              name="name"
              type="text"
            />
            <p className="text-red-500 text-xs italic">
              <ErrorMessage name="name" />
            </p>
          </div>
          <div className="mb-4">
            <Field
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
              id="income"
              placeholder="Income"
              name={"income"}
              type="number"
            />
            <p className="text-red-500 text-xs italic">
              <ErrorMessage name={"income"} />
            </p>
          </div>
          <div className="flex items-center justify-between">
            <button
              onClick={onCloseModal}
              className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded"
            >
              Cancel
            </button>
            <button
              className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
              type="submit"
            >
              {buttonActionName}
            </button>
          </div>
        </Form>
      </div>
    </Formik>
  );
};

export default AddBudgetForm;
