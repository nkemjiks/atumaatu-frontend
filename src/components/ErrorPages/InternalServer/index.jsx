import React from "react";
import styled from "styled-components";
import "./style.scss";

const Container = styled.div`
  font-family: monospace;
  color: black;
  background-color: white;
  height: 100vh;
  display: flex;
  flex-direction: column;

  div {
    max-width: 800px;
    font-size: 16px;
    margin: auto;

    p {
      margin-bottom: 10px;
    }
  }
`;

const InternalServer = () => (
  <Container>
    <div>
      <p>
        A problem has been detected and and the website has been shut down to
        prevent damage to your eyes and brain.
      </p>
      <p>500 INTERNAL SERVER ERROR</p>
      <p>
        If this is the first time you've seen this error screen, restart your
        computer. If this screen appears again, follow these steps:
      </p>
      <p>
        Actually, there are no steps to follow. Try loading this page again in a
        day, a week, or a year.
      </p>
      <p>Technical information:</p>
      <p>*** Nothing to see here</p>
    </div>
  </Container>
);

export default InternalServer;
