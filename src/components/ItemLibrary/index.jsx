import React, { useEffect, useContext, useState } from 'react';
import { observer } from 'mobx-react';
import {withRouter} from 'react-router';
import * as yup from "yup";
import { FaTrash, FaEdit } from "react-icons/fa";
import { UserStoreContext } from "../../stores/UserStore";
import { ItemsStoreContext } from "../../stores/ItemsStore";
import Loading from '../shared/LoadingOverlay';
import AddItem from './AddItem';
import EditItem from './EditItem';
import DeleteItem from './DeleteItem';

const validationSchema = yup.object({
  name: yup
    .string()
    .min(3, 'Name must be greater than 2')
    .required("Required")
});

const ItemLibrary = observer(({ match }) => {
  const userStore = useContext(UserStoreContext);
  const itemsStore = useContext(ItemsStoreContext);
  const [openAdd, setOpenAdd] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [itemInfo, setItemInfo] = useState({ name: '', id: 1 })

  const { kind, errors, items } = itemsStore.state;

  useEffect(() => {
    userStore.getUserFromLocalStorage(match.url);
    document.title = 'Item Library - Atumaatu'
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    itemsStore.updateTokenState(userStore.state.user.token);
    itemsStore.getItems();
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userStore.state.user.token]);

  const onOpenAddModal = () => {
    setOpenAdd(true);
  };

  const onCloseAddModal = () => {
    setOpenAdd(false);
  };

  const onOpenEditModal = () => {
    setOpenEdit(true);
  };

  const onCloseEditModal = () => {
    setOpenEdit(false);
  };

  const onOpenDeleteModal = () => {
    setOpenDelete(true);
  };

  const onCloseDeleteModal = () => {
    setOpenDelete(false);
  };

  const handleAdd = () => {
    onOpenAddModal();
  }

  const handleAddSubmit = (detail) => {
    itemsStore.addItem(detail);
    onCloseAddModal()
  }

  const handleEdit = (item) => {
    setItemInfo(item);
    onOpenEditModal();
  }

  const handleEditSubmit = (detail) => {
    itemsStore.editItem(itemInfo.id, detail);
    onCloseEditModal()
  }

  const handleDelete = (item) => {
    setItemInfo(item);
    onOpenDeleteModal();
  }

  const handleDeleteSubmit = () => {
    itemsStore.removeItem(itemInfo.id);
    onCloseDeleteModal()
  }

  return kind === "loading" ?
    <Loading /> :
    (
      <div className="bg-gray-100 min-h-screen px-12 py-2">
        <AddItem
          title={'Add Item'}
          open={openAdd}
          onCloseModal={onCloseAddModal}
          initialValues={{ name: '' }}
          validationSchema={validationSchema}
          handleSubmit={handleAddSubmit}
        />
        <EditItem
          title={'Edit Item'}
          open={openEdit}
          onCloseModal={onCloseEditModal}
          initialValues={{ name: itemInfo.name }}
          validationSchema={validationSchema}
          handleSubmit={handleEditSubmit}
        />
        <DeleteItem
          title={itemInfo.name}
          open={openDelete}
          onCloseModal={onCloseDeleteModal}
          handleSubmit={handleDeleteSubmit}
        />
        <div className="flex justify-end py-8">
          <button onClick={handleAdd}
            className="bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 mr-2 border border-gray-400 rounded shadow"
          >
            + Add an item
          </button>
        </div>
        <h3 className="font-bold text-4xl mb-2">Item Library</h3>

        {errors && errors.errorMessage && (
          <p className=" text-center mb-2 text-red-500 text-xs italic">
            {errors.errorMessage}
          </p>
        )}
        <div>
          {
            items.length ===  0 ? (
              <div>You don't have any items in your library. Add some with the button above</div>
            ) :
            (
              <table className="table-auto w-full">
                <thead>
                  <tr>
                    <th className="w-1/2 px-4 py-2">Name</th>
                    <th className="w-1/2 px-4 py-2">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    items.map((item, i) =>  {
                      return (
                        <tr key={i}>
                          <td className="border px-4 py-2 text-center">{item.name}</td>
                          <td className="border px-4 py-2 text-center">
                            <span onClick={() => handleEdit({ name: item.name, id: item.id })}
                              className="cursor-pointer inline-block rounded-full px-1 py-2 text-sm font-semibold text-right text-blue-700 mr-2"
                            >
                              <FaEdit />
                            </span>
                            {
                              item.deletable ? (<span onClick={() => handleDelete({ name: item.name, id: item.id })}
                                className="cursor-pointer inline-block rounded-full px-1 py-2 text-sm font-semibold text-right text-red-700 mr-2"
                              >
                                <FaTrash />
                              </span>) : null
                            }
                          </td>
                        </tr>
                      )}
                    )
                  }
                </tbody>
              </table>
            )
          }
        </div>
      </div>
    )
});

export default withRouter(ItemLibrary);