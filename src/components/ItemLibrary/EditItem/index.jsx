import React from "react";
import AppModal from "../../shared/Modal";
import EditItemForm from "./EditItemForm";

const EditItem = ({ title, open, onCloseModal, handleSubmit, initialValues, validationSchema }) => {
  return (
    <AppModal
      title={title}
      open={open}
      onCloseModal={onCloseModal}
    >
      <EditItemForm
        initialValues={initialValues}
        validationSchema={validationSchema}
        handleSubmit={handleSubmit}
      />
    </AppModal>
  );
};

export default EditItem;
