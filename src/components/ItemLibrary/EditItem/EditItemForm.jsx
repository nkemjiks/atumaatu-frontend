import React from "react";
import { Form, Formik, Field, ErrorMessage } from "formik";

const EditItemForm = ({
  initialValues,
  validationSchema,
  handleSubmit,
  itemName
}) => {
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={handleSubmit}
    >
      {() => {
        return (
          <Form className="bg-white px-8 pt-6 pb-8 mb-4">
            <div className="mb-4">
              <label
                className="block text-gray-700 text-sm mb-2"
                htmlFor="email"
              >
                {itemName}
              </label>
              <Field
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                id="name"
                placeholder="Item"
                name="name"
                type="text"
              />
              <p className="text-red-500 text-xs italic">
                <ErrorMessage name="name" />
              </p>
            </div>
            <div className="flex items-center justify-end">
              <button
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                type="submit"
              >
                Update Item
              </button>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

export default EditItemForm;
