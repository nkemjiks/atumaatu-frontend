import React from "react";
import AppModal from "../../shared/Modal";
import AddItemForm from "./AddItemForm";

const AddItem = ({ title, open, onCloseModal, handleSubmit, initialValues, validationSchema }) => {
  return (
    <AppModal
      title={title}
      open={open}
      onCloseModal={onCloseModal}
    >
      <AddItemForm
        initialValues={initialValues}
        validationSchema={validationSchema}
        handleSubmit={handleSubmit}
      />
    </AppModal>
  );
};

export default AddItem;
