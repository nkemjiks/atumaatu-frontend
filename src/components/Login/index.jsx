import React, { useContext, useEffect } from "react";
import styled from "styled-components";
import { Form, Formik, Field, ErrorMessage } from "formik";
import * as yup from "yup";
import { observer } from "mobx-react";
import { UserStoreContext } from "../../stores/UserStore";
import budgetImage from "../../assets/budget.jpg";
import Loading from "../shared/LoadingOverlay";

const initialValues = {
  email: "",
  password: ""
};

const validationSchema = yup.object({
  email: yup
    .string()
    .email("Invalid email address")
    .required("Email address is required"),
  password: yup.string().required("Password is required")
});

const Container = styled.div`
  width: 100%;
`;

const ImageContainer = styled.div`
  background: linear-gradient(rgba(0, 0, 0, 0.86),rgba(0, 0, 0, 0.78)), url(${budgetImage});
  background-size: cover;
  background-position: center;
  justify-content: center;
`

const Login = observer(({ history }) => {
  const userStore = useContext(UserStoreContext);
  const { errors } = userStore.state;

  useEffect(() => {
    userStore.getUserFromLocalStorage();
    document.title = 'Login - Atumaatu'
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleSubmit = event => {
    userStore.loginUser(event);
  };

  return userStore.state.kind === "loading" ? (
    <Loading />
  ) : (
    <Container>
      <div className="flex h-screen">
        <ImageContainer className="bg-scroll text-white bg-cover bg-no-repeat bg-center flex-col flex content-center flex-1 text-center bg-red-700 px-10 py-2">
          <h2 className="text-white text-6xl font-semibold tracking-wider">Atumaatu</h2>
          <p className="text-white text-xl">Budgeting for everyone</p>
        </ImageContainer>
        <div className="flex-1 flex text-black bg-gray-100 px-4 py-2">
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
          >
            <div className="w-4/5 mx-auto self-center">
              <h5 className="text-3xl mb-6">Login</h5>
              <Form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
                {errors && errors.errorMessage && (
                  <p className=" text-center mb-2 text-red-500 text-xs italic">
                    {errors.errorMessage}
                  </p>
                )}
                <div className="mb-4">
                  <label
                    className="block text-gray-700 text-sm mb-2"
                    htmlFor="email"
                  >
                    Email
                  </label>
                  <Field
                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                    id="email"
                    placeholder="Email"
                    name="email"
                    type="email"
                  />
                  <p className="text-red-500 text-xs italic">
                    <ErrorMessage name="email" />
                  </p>
                </div>
                <div className="mb-6">
                  <label
                    className="block text-gray-700 text-sm mb-2"
                    htmlFor="password"
                  >
                    Password
                  </label>
                  <Field
                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                    id="password"
                    type="password"
                    name="password"
                    placeholder="******************"
                  />
                  <p className="text-red-500 text-xs italic">
                    <ErrorMessage name="password" />
                  </p>
                </div>
                <div className="flex items-center justify-between">
                  <button
                    className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                    type="submit"
                  >
                    Sign In
                  </button>
                  <a className="inline-block align-baseline font-bold text-sm text-pink-800 hover:text-blue-800" href="/signup">
                    Register
                  </a>
                </div>
              </Form>
            </div>
          </Formik>
        </div>
      </div>
    </Container>
  );
});

export default Login;
