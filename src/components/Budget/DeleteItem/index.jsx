import React from "react";
import AppModal from "../../shared/Modal";
import DeleteItemConfirmation from "./DeleteItem";

const DeleteItem = ({ title, open, onCloseModal, handleSubmit }) => {
  return (
    <AppModal
      title={title}
      open={open}
      onCloseModal={onCloseModal}
    >
      <DeleteItemConfirmation
        handleSubmit={handleSubmit}
        onCloseModal={onCloseModal}
      />
    </AppModal>
  );
};

export default DeleteItem;
