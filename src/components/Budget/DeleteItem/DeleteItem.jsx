import React from "react";

const DeleteItemConfirmation = ({ onCloseModal, handleSubmit }) => {
  return (
    <div className="w-full mx-auto self-center">
      <p className="mb-4">Are you sure you want to delete this item</p>
      <div className="flex items-center justify-between">
        <button
          onClick={onCloseModal}
          className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded"
        >
          No
        </button>
        <button
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
          onClick={handleSubmit}
        >
          Yes
        </button>
      </div>
    </div>
  );
};

export default DeleteItemConfirmation;
