import React from "react";
import AppModal from "../../shared/Modal";
import EditItemForm from "./EditItemForm";

const EditItem = ({ title, open, onCloseModal, handleSubmit, initialValues, validationSchema, errors }) => {
  return (
    <AppModal
      title={title}
      open={open}
      onCloseModal={onCloseModal}
    >
      <EditItemForm
        initialValues={initialValues}
        validationSchema={validationSchema}
        handleSubmit={handleSubmit}
        errors={errors}
      />
    </AppModal>
  );
};

export default EditItem;
