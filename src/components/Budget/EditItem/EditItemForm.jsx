import React from "react";
import { Form, Formik, Field, ErrorMessage } from "formik";

const EditItemForm = ({
  initialValues,
  validationSchema,
  handleSubmit,
  errors,
  itemName
}) => {
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={handleSubmit}
    >
      {({ values }) => {
        return (
          <Form className="bg-white px-8 pt-6 pb-8 mb-4">
            {errors && errors.errorMessage && (
              <p className=" text-center mb-2 text-red-500 text-xs italic">
                {errors.errorMessage}
              </p>
            )}
            <div>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm mb-2"
                  htmlFor="budgeted_cost"
                >
                  Budgeted cost
                </label>
                <Field
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                  id="budgeted_cost"
                  placeholder="0"
                  name="budgeted_cost"
                  type="number"
                />
                <p className="text-red-500 text-xs italic">
                  <ErrorMessage name="budgeted_cost" />
                </p>
              </div>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm mb-2"
                  htmlFor="executed"
                >
                  Executed
                </label>
                <Field
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                  id="executed"
                  name="executed"
                  type="checkbox"
                  checked={values.executed}
                  component={Checkbox}
                />
                <p className="text-red-500 text-xs italic">
                  <ErrorMessage name="executed" />
                </p>
              </div>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm mb-2"
                  htmlFor="actual_cost"
                >
                  Actual cost
                </label>
                <Field
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                  id="actual_cost"
                  placeholder="0"
                  name="actual_cost"
                  type="number"
                />
                <p className="text-red-500 text-xs italic">
                  <ErrorMessage name="actual_cost" />
                </p>
              </div>
            </div>
            <div className="flex items-center justify-between">
              <button
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                type="submit"
              >
                Update Item
              </button>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

function Checkbox({ field, type, checked }) {
  return (
    <label>
      {/* remove {...field} to see changes not propagated */}
      <input {...field} type={type} checked={checked} />
    </label>
  );
}

export default EditItemForm;
