import React, { useContext, useEffect, useState } from "react";
import { observer } from "mobx-react";
import { withRouter } from "react-router";
import * as yup from "yup";
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import {
  FaTrash,
  FaEdit,
  FaPlus,
  FaDownload,
  FaCheck,
  FaTimes,
} from "react-icons/fa";
import { UserStoreContext } from "../../stores/UserStore";
import { BudgetItemsStoreContext } from "../../stores/BudgetItemsStore";
import Loading from "../shared/LoadingOverlay";
import { cost } from "../../helpers/calc";
import AddItems from "./AddItems";
import EditItem from "./EditItem";
import DeleteItem from "./DeleteItem";
import { docDefinition } from "../../helpers/pdfGenarator";
import Difference from "../shared/Difference";

pdfMake.vfs = pdfFonts.pdfMake.vfs;

let initialValues = {
  items: [{ name: "", budgeted_cost: undefined }],
};

yup.addMethod(yup.array, "unique", function (message, mapper = (a) => a) {
  return this.test("unique", message, function (list) {
    return list.length === new Set(list.map(mapper)).size;
  });
});

const validationSchema = yup.object().shape({
  items: yup
    .array()
    .of(
      yup.object().shape({
        name: yup
          .string()
          .min(2, "Name is too short. Must be greater than 2")
          .required("Required"),
        budgeted_cost: yup
          .number()
          .min(1, "Budgeted cost must be greater than zero")
          .required("Required"),
      })
    )
    .unique("Duplicate names found", (a) => a.name)
    .required("Must have Items")
    .min(1, "Minimum of 3 Items"),
});

const validationEditSchema = yup.object({
  budgeted_cost: yup
    .number()
    .min(1, "Budgeted cost must be greater than zero")
    .required("Budgeted cost is required"),
  executed: yup.boolean(),
  actual_cost: yup.number().when("executed", {
    is: true,
    then: yup.number().min(1),
    otherwise: yup.number().min(0).max(0),
  }),
});

const Budget = observer(({ match }) => {
  const userStore = useContext(UserStoreContext);
  const budgetItemsStore = useContext(BudgetItemsStoreContext);
  const { errors, kind, budget, items } = budgetItemsStore.state;
  const [open, setOpen] = useState(false);
  const [budgetId, setBudgetId] = useState(0);
  const [openEdit, setOpenEdit] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [editable, setEditable] = useState({
    name: "",
    budgeted_cost: 0,
    executed: false,
    actual_cost: 0,
    bud_mem_id: 1,
  });
  const [deletable, setDeletable] = useState({ name: "", bud_mem_id: 1 });

  useEffect(() => {
    userStore.getUserFromLocalStorage(match.url);
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    budgetItemsStore.updateTokenState(userStore.state.user.token);
    budgetItemsStore.getUserBudgetItems(match.params.id);
    setBudgetId(match.params.id);
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userStore.state.user.token]);

  useEffect(() => {
    if (budget.name) {
      document.title = `${budget.name} - Atumaatu`;
    }
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [budgetItemsStore.state.budget.name]);

  const onOpenModal = () => {
    setOpen(true);
  };

  const onCloseModal = () => {
    setOpen(false);
  };

  const onOpenEditModal = () => {
    setOpenEdit(true);
  };

  const onCloseEditModal = () => {
    setOpenEdit(false);
  };

  const onOpenDeleteModal = () => {
    setOpenDelete(true);
  };

  const onCloseDeleteModal = () => {
    setOpenDelete(false);
  };

  const handleSubmit = (details) => {
    budgetItemsStore.addItems({ id: budgetId, items: details.items });
    onCloseModal();
  };

  const handleEditSubmit = (detail) => {
    budgetItemsStore.editItem(budgetId, {
      budgeted_cost: detail.budgeted_cost,
      actual_cost: detail.actual_cost,
      executed: detail.executed,
      bud_mem_id: editable.bud_mem_id,
    });
    onCloseEditModal();
  };

  const handleEdit = (item) => {
    setEditable(item);
    onOpenEditModal();
  };

  const handleDelete = (item) => {
    setDeletable(item);
    onOpenDeleteModal();
  };

  const handleDeleteSubmit = () => {
    budgetItemsStore.removeItem(budgetId, { bud_mem_id: deletable.bud_mem_id });
    onCloseDeleteModal();
  };

  const downloadPdf = (title, budgetCost, currency, items) => {
    const itemsTable = items.map((item) => {
      return [
        {
          border: [false, false, false, true],
          fillColor: "#eeeeee",
          text: `${item.name}`,
        },
        {
          border: [false, false, false, true],
          fillColor: "#dddddd",
          text: `${cost(currency, item.budgetedCost)}`,
        },
      ];
    });
    const dd = docDefinition(title, budgetCost, currency, itemsTable);
    pdfMake.createPdf(dd).download(title);
  };

  return kind === "loading" ? (
    <Loading />
  ) : (
    <div className="bg-gray-100 min-h-screen px-12 py-2">
      <AddItems
        title={"Add Items"}
        open={open}
        onCloseModal={onCloseModal}
        initialValues={initialValues}
        validationSchema={validationSchema}
        handleSubmit={handleSubmit}
        errors={errors}
      />
      <EditItem
        title={editable.name}
        open={openEdit}
        onCloseModal={onCloseEditModal}
        initialValues={{
          budgeted_cost: editable.budgeted_cost,
          executed: editable.executed,
          actual_cost: editable.actual_cost,
        }}
        validationSchema={validationEditSchema}
        handleSubmit={handleEditSubmit}
        errors={errors}
      />
      <DeleteItem
        title={deletable.name}
        open={openDelete}
        onCloseModal={onCloseDeleteModal}
        handleSubmit={handleDeleteSubmit}
      />
      <div className="flex justify-end py-8">
        <button
          onClick={onOpenModal}
          className="bg-white hover:bg-gray-100 text-gray-800 font-semibold flex py-2 px-4 mr-2 border border-gray-400 rounded shadow"
        >
          <FaPlus className="mr-2 mt-1" /> Add an item
        </button>
        {items.length > 0 && (
          <button
            onClick={() =>
              downloadPdf(
                budget.name,
                budget.budgeted_cost,
                userStore.state.user.currency,
                items
              )
            }
            className="bg-white hover:bg-gray-100 text-gray-800 font-semibold flex py-2 px-4 mr-2 border border-gray-400 rounded shadow"
          >
            <FaDownload className="mr-2 mt-1" /> Download as PDF
          </button>
        )}
      </div>
      <h3 className="font-bold text-4xl mb-2">{budget.name}</h3>
      <p>
        Total cost: {cost(userStore.state.user.currency, budget.budgeted_cost)}
      </p>
      <div className="text-center px-4 py-4 bg-indigo-200 mt-4 mb-4">
        <p>You have {items.length} items in this budget</p>
      </div>

      {errors && errors.errorMessage && (
        <p className=" text-center mb-2 text-red-500 text-xs italic">
          {errors.errorMessage}
        </p>
      )}
      <div>
        {items.length === 0 ? (
          <div>You don't have any items in this budget</div>
        ) : (
          <table className="table-auto w-full">
            <thead>
              <tr>
                <th className="w-1/6 px-4 py-2">Name</th>
                <th className="w-1/6 px-4 py-2">Budgeted Cost</th>
                <th className="w-1/6 px-4 py-2">Executed</th>
                <th className="w-1/6 px-4 py-2">Actual Cost</th>
                <th className="w-1/6 px-4 py-2">Difference</th>
                <th className="w-1/6 px-4 py-2">Action</th>
              </tr>
            </thead>
            <tbody>
              {items.map((item, i) => {
                return (
                  <tr key={i}>
                    <td className="border px-4 py-2 text-center">
                      {item.name}
                    </td>
                    <td className="border px-4 py-2 text-center">
                      {cost(userStore.state.user.currency, item.budgetedCost)}
                    </td>
                    <td className="border px-4 py-2 text-center">
                      {item.executed ? (
                        <span className="cursor-pointer inline-block rounded-full px-1 py-2 text-sm font-semibold text-right text-green-700 mr-2">
                          <FaCheck />
                        </span>
                      ) : (
                        <span className="cursor-pointer inline-block rounded-full px-1 py-2 text-sm font-semibold text-right text-red-700 mr-2">
                          <FaTimes />
                        </span>
                      )}
                    </td>
                    <td className="border px-4 py-2 text-center">
                      {cost(userStore.state.user.currency, item.actualCost)}
                    </td>
                    <td className="border px-4 py-2 text-center">
                      <Difference item={item} costDifference={cost(userStore.state.user.currency, (item.budgetedCost - item.actualCost))} />
                    </td>
                    <td className="border px-4 py-2 text-center">
                      <span
                        onClick={() =>
                          handleEdit({
                            name: item.name,
                            budgeted_cost: item.budgetedCost,
                            executed: item.executed,
                            actual_cost: item.actualCost,
                            bud_mem_id: item.bud_mem_id,
                          })
                        }
                        className="cursor-pointer inline-block rounded-full px-1 py-2 text-sm font-semibold text-right text-blue-700 mr-2"
                      >
                        <FaEdit />
                      </span>
                      <span
                        onClick={() =>
                          handleDelete({
                            name: item.name,
                            bud_mem_id: item.bud_mem_id,
                          })
                        }
                        className="cursor-pointer inline-block rounded-full px-1 py-2 text-sm font-semibold text-right text-red-700 mr-2"
                      >
                        <FaTrash />
                      </span>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        )}
      </div>
    </div>
  );
});

export default withRouter(Budget);
