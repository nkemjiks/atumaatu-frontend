import React from "react";
import AppModal from "../../shared/Modal";
import BudgetItemsForm from "./BudgetItemsForm";

const AddItems = ({ title, open, onCloseModal, handleSubmit, initialValues, validationSchema, errors }) => {
  return (
    <AppModal
      title={title}
      open={open}
      onCloseModal={onCloseModal}
    >
      <BudgetItemsForm
        initialValues={initialValues}
        validationSchema={validationSchema}
        handleSubmit={handleSubmit}
        errors={errors}
      />
    </AppModal>
  );
};

export default AddItems;
