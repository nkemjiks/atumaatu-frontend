import React from "react";
import { Form, Formik, Field, FieldArray, getIn } from "formik";
// import CreatableSelect from 'react-select/creatable';
import { FaPlus, FaMinus } from "react-icons/fa";

const ErrorMessage = ({ name }) => (
  <Field
    name={name}
    render={({ form }) => {
      const error = getIn(form.errors, name);
      const touch = getIn(form.touched, name);
      return touch && error ? error : null;
    }}
  />
);

const AddItemsForm = ({
  initialValues,
  validationSchema,
  handleSubmit,
}) => {
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={handleSubmit}
    >
      {({ values }) => {
        return (
          <Form className="bg-white px-8 pt-6 pb-8 mb-4">
            <FieldArray
              name="items"
              render={arrayHelpers => (
                <div>
                  <p className="text-red-500 text-xs text-center mb-2 italic">
                    {typeof arrayHelpers.form.errors.items === "string"
                      ? arrayHelpers.form.errors.items
                      : null}
                  </p>
                  {values.items && values.items.length > 0 ? (
                    values.items.map((item, index) => (
                      <div key={index} className="flex justify-between">
                        <div className="mb-4 px-4">
                          <Field
                            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                            id="name"
                            placeholder="Name"
                            name={`items.${index}.name`}
                            type="text"
                          />
                          <p className="text-red-500 text-xs italic">
                            <ErrorMessage name={`items[${index}].name`} />
                          </p>
                        </div>
                        <div className="mb-4 px-4">
                          <Field
                            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                            id="budgeted_cost"
                            placeholder="Budgeted Cost"
                            name={`items.${index}.budgeted_cost`}
                            type="number"
                          />
                          <p className="text-red-500 text-xs italic">
                            <ErrorMessage name={`items[${index}].budgeted_cost`} />
                          </p>
                        </div>
                        <div className="mt-2">
                          <button
                            type="button"
                            className="px-2 py-2"
                            onClick={() => arrayHelpers.remove(index)}
                          >
                            <FaMinus />
                          </button>
                          <button
                            type="button"
                            className="px-2 py-2"
                            onClick={() =>
                              arrayHelpers.push({ name: "", budgeted_cost: undefined })
                            }
                          >
                            <FaPlus />
                          </button>
                        </div>
                      </div>
                    ))
                  ) : (
                    <button
                      type="button"
                      onClick={() => arrayHelpers.push({ name: "", budgeted_cost: 0 })}
                    >
                      Add an Item
                    </button>
                  )}
                  <div className="flex items-center justify-end">
                    <button
                      className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                      type="submit"
                    >
                      Submit
                    </button>
                  </div>
                </div>
              )}
            />
          </Form>
        );
      }}
    </Formik>
  );
};

// const MyCheckbox = ({ field, form, label, ...rest }) => {
//   const { name, value: formikValue } = field;
//   const { setFieldValue } = form;

//   // const handleChange = event => {
//   //   const values = formikValue || [];
//   //   const index = values.indexOf(rest.value);
//   //   if (index === -1) {
//   //     values.push(rest.value);
//   //   } else {
//   //     values.splice(index, 1);
//   //   }
//   //   setFieldValue(name, values);
//   // };

//   const handleChange = (newValue, actionMeta) => {
//     console.group('Value Changed');
//     console.log(newValue);
//     console.log(`action: ${actionMeta.action}`);
//     console.groupEnd();
//     const values = formikValue || [];
//     const index = values.indexOf(rest.value);
//     if (index === -1) {
//       values.push(rest.value);
//     } else {
//       values.splice(index, 1);
//     }
//     setFieldValue(name, values);
//   };

//   const handleInputChange = (inputValue, actionMeta) => {
//     console.group('Input Changed');
//     console.log(inputValue);
//     console.log(`action: ${actionMeta.action}`);
//     console.groupEnd();
//   };

//   return (
//     <CreatableSelect
//       isClearable={false}
//       onChange={handleChange}
//       onInputChange={handleInputChange}
//       options={colourOptions}
//     />
//   );
// };

export default AddItemsForm;
