import React, { useState, useContext } from "react";
import styled from "styled-components";
import { FaCaretDown } from "react-icons/fa";
import { observer } from "mobx-react";
import { UserStoreContext } from "../../stores/UserStore";

const DropDownContainer = styled.div`
  position: fixed;
  top: 65px;
  display: ${props => props.showDropDown ? 'flex' : 'none'};
  flex-direction: column;
  align-items: baseline;
  padding: 0;
  width: 250px;
  right: 0;
  z-index: 3;
  background-color: #495c82;
`;

const Container = styled.div`
  max-width: 100%;
  color: white;
`;

const MainContainer = styled.div`
  width: 100%;
  box-shadow: rgba(0, 0, 0, 0.05) 0px 1px;
  transition: box-shadow cubic-bezier(.165, .84, .44, 1) .25s;
  display: block;
`;

const Para = styled.p`
  cursor: pointer;
  display: flex;
  align-items: center;
`

const Navbar = observer(() => {
  const [showDropDown, setShowDropDown] = useState(false);
  const userStore = useContext(UserStoreContext);

  const logOutUser = () => {
    userStore.logOutUser();
  }

  return (
    <MainContainer>
      <Container className="h-16 bg-gray-900 flex max-w-full justify-between px-8 py-2 items-center">
        <h2 className="text-2xl font-semibold tracking-wider">Atumaatu</h2>
        <Para onClick={() => setShowDropDown(!showDropDown)}>Hi {userStore.state.user.firstName}  <FaCaretDown/></Para>
        <DropDownContainer showDropDown={showDropDown} className="border border-gray-200">
          <a href="/dashboard" className="py-3 hover:bg-gray-200 hover:text-black px-4 w-full text-sm">Dashboard</a>
          <a href="/items-library" className="py-3 px-4 w-full hover:bg-gray-200 hover:text-black text-sm">Items Library</a>
          <a href="/profile" className="py-3 px-4 w-full hover:bg-gray-200 hover:text-black text-sm">Profile</a>
          <button onClick={logOutUser} className="py-3 text-black px-4 w-full hover:bg-gray-300 bg-gray-100 text-sm">Logout</button>
        </DropDownContainer>
      </Container>
    </MainContainer>
  )
});

export default Navbar;
