import React, { useEffect, useContext } from "react";
import styled from "styled-components";
import { Form, Formik, Field, ErrorMessage } from "formik";
import * as yup from "yup";
import { observer } from "mobx-react";
import currencies from "./currency.json";
import budgetImage from "../../assets/budget.jpg";
import { UserStoreContext } from "../../stores/UserStore";
import Loading from "../shared/LoadingOverlay";

const initialValues = {
  firstname: "",
  lastname: "",
  email: "",
  password: "",
  password_confirmation: "",
  currency: "NGN"
};

const validationSchema = yup.object({
  firstname: yup
    .string()
    .max(256, "First name must be 256 characters or less")
    .required("First name is required"),
  lastname: yup
    .string()
    .max(256, "Last name must be 256 characters or less")
    .required("Last name is required"),
  email: yup
    .string()
    .email("Invalid email address")
    .required("Email address is required"),
  currency: yup.string().required("Currency is required"),
  password: yup.string().required("Password is required"),
  password_confirmation: yup
    .string()
    .oneOf([yup.ref("password"), null], "Passwords do not match")
    .required("Passwords do not match")
});

const Container = styled.div`
  width: 100%;
`;

const ImageContainer = styled.div`
  background: linear-gradient(rgba(0, 0, 0, 0.86),rgba(0, 0, 0, 0.78)), url(${budgetImage});
  background-size: cover;
  background-position: center;
  justify-content: center;
`

const SignUp = ({ history }) => {
  const userStore = useContext(UserStoreContext);
  const { errors } = userStore.state;

  useEffect(() => {
    userStore.getUserFromLocalStorage();
    document.title = 'Register - Atumaatu'
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleSubmit = event => {
    userStore.signUpUser(event);
  };

  return userStore.state.kind === "loading" ? (
    <Loading />
  ) : (
    <Container>
      <div className="flex h-screen">
        <ImageContainer className="bg-scroll text-white bg-cover bg-no-repeat bg-center flex-col flex content-center flex-1 text-center bg-red-700 px-10 py-2">
          <h2 className="text-white text-6xl font-semibold tracking-wider">Atumaatu</h2>
          <p className="text-white text-xl">Budgeting for everyone</p>
        </ImageContainer>
        <div className="flex-1 flex bg-gray-100 px-4 py-2">
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
          >
            <div className="w-4/5 mx-auto self-center">
              <h5 className="text-3xl mb-6 text-black">Register</h5>
              <Form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
                {errors && errors.errorMessage && (
                  <p className=" text-center mb-2 text-red-500 text-xs italic">
                    {errors.errorMessage}
                  </p>
                )}
                <div className="mb-4">
                  <label
                    className="block text-gray-700 text-sm mb-2"
                    htmlFor="firstname"
                  >
                    First name
                  </label>
                  <Field
                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                    id="firstname"
                    placeholder="First Name"
                    name="firstname"
                    type="text"
                  />
                  <p className="text-red-500 text-xs italic">
                    <ErrorMessage name="firstname" />
                  </p>
                </div>
                <div className="mb-4">
                  <label
                    className="block text-gray-700 text-sm mb-2"
                    htmlFor="lastname"
                  >
                    Last name
                  </label>
                  <Field
                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                    id="lastname"
                    placeholder="Last Name"
                    name="lastname"
                    type="text"
                  />
                  <p className="text-red-500 text-xs italic">
                    <ErrorMessage name="lastname" />
                  </p>
                </div>
                <div className="mb-4">
                  <label
                    className="block text-gray-700 text-sm mb-2"
                    htmlFor="email"
                  >
                    Email
                  </label>
                  <Field
                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                    id="email"
                    placeholder="Email"
                    name="email"
                    type="email"
                  />
                  <p className="text-red-500 text-xs italic">
                    <ErrorMessage name="email" />
                  </p>
                </div>
                <div className="mb-4">
                  <label
                    className="block text-gray-700 text-sm mb-2"
                    htmlFor="currency"
                  >
                    Currency
                  </label>
                  <Field
                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                    id="currency"
                    name="currency"
                    as="select"
                  >
                    {currencies.map((currency, i) => {
                      return (
                        <option key={i} value={currency.symbol}>
                          {currency.name}
                        </option>
                      );
                    })}
                  </Field>
                  <p className="text-red-500 text-xs italic">
                    <ErrorMessage name="currency" />
                  </p>
                </div>
                <div className="mb-6">
                  <label
                    className="block text-gray-700 text-sm mb-2"
                    htmlFor="password"
                  >
                    Password
                  </label>
                  <Field
                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                    id="password"
                    type="password"
                    name="password"
                    placeholder="******************"
                  />
                  <p className="text-red-500 text-xs italic">
                    <ErrorMessage name="password" />
                  </p>
                </div>
                <div className="mb-6">
                  <label
                    className="block text-gray-700 text-sm mb-2"
                    htmlFor="password_confirmation"
                  >
                    Confirm Password
                  </label>
                  <Field
                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                    id="password_confirmation"
                    type="password"
                    name="password_confirmation"
                    placeholder="******************"
                  />
                  <p className="text-red-500 text-xs italic">
                    <ErrorMessage name="password_confirmation" />
                  </p>
                </div>
                <div className="flex items-center justify-between">
                  <button
                    className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                    type="submit"
                  >
                    Register
                  </button>
                  <a className="inline-block align-baseline font-bold text-sm text-pink-800 hover:text-blue-800" href="/">
                    Login
                  </a>
                </div>
              </Form>
            </div>
          </Formik>
        </div>
      </div>
    </Container>
  );
};

export default observer(SignUp);
