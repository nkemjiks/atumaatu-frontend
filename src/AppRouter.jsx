import React from 'react';
import { Route, Switch } from 'react-router-dom';
import NotFound from './components/ErrorPages/NotFound';
import Dashboard from './components/Dashboard';
import Budget from './components/Budget';
import Navbar from './components/Navbar';
import ItemLibrary from './components/ItemLibrary'
import Profile from './components/Profile';
import { UserProtected } from './Protected';

const AppRouter = () => {
  return (
    <>
      <Navbar/>
      <Switch>
        <UserProtected exact path="/dashboard" component={Dashboard}/>
        <UserProtected exact path="/budget/:id" component={Budget}/>
        <UserProtected exact path="/items-library" component={ItemLibrary}/>
        <UserProtected exact path="/profile" component={Profile}/>
        <Route path="*" component={NotFound} />
      </Switch>
    </>
  );
}

export default AppRouter;